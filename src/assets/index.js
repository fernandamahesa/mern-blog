import RegisterBg from "./images/registerbg.jpg";
import LoginBg from "./images/loginbg.jpg";
import LogoReact from "./images/logo.svg";

//icons
import ICFacebook from "./icons/facebook.svg";
import ICTwitter from "./icons/twitter.svg";
import ICInstagram from "./icons/instagram.svg";
import ICGithub from "./icons/github.svg";

export {
  RegisterBg,
  LoginBg,
  LogoReact,
  ICFacebook,
  ICTwitter,
  ICInstagram,
  ICGithub,
};
