import Register from "./Register";
import Login from "./Login";
import MainApp from "./MainApp";

export { Register, Login, MainApp };
