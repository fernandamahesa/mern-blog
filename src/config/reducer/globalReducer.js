const initialState = {
  name: "fernanda mahesa",
};

const globalReducer = (state = initialState, action) => {
  if (action.type === "UPDATE_NAME") {
    return {
      ...state,
      name: "karinia",
    };
  }
  return state;
};

export default globalReducer;
