import React from "react";
import {
  ICFacebook,
  ICGithub,
  ICInstagram,
  ICTwitter,
  LogoReact,
} from "../../../assets";
import "./footer.scss";

const Icon = ({ img }) => {
  return (
    <div className="icon-wrapper">
      <img className="icon-medsos" src={img} alt="icon" />
    </div>
  );
};

function Footer() {
  return (
    <div>
      <div className="footer">
        <img className="logo" src={LogoReact} alt="logo" />
        <div className="social-wrapper">
          <Icon img={ICFacebook} />
          <Icon img={ICTwitter} />
          <Icon img={ICInstagram} />
          <Icon img={ICGithub} />
        </div>
      </div>
      <div className="copyright">
        <p>Copyright @2020</p>
      </div>
    </div>
  );
}

export default Footer;
