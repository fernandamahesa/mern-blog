import React from "react";
import { useHistory } from "react-router-dom";
import "./header.scss";

function Header() {
  const history = useHistory();
  return (
    <div className="header">
      <p className="logo-app">MERN-Blog</p>
      <p className="menu-items" onClick={() => history.push("/login")}>
        Logout
      </p>
    </div>
  );
}

export default Header;
