import React from "react";
import "./textArea.scss";

function TextArea({ ...rest }) {
  return (
    <div>
      <textarea className="text-area" {...rest} />
    </div>
  );
}

export default TextArea;
