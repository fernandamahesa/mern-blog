import React from "react";
import "./upload.scss";

function Upload({ img, ...rest }) {
  return (
    <div className="upload">
      {img && <img className="preview" src={img} alt="preview" />}
      <input type="file" {...rest} />
    </div>
  );
}

export default Upload;
